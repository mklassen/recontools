//
// Created by Martyn Klassen on 2019-12-05
//

#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include "recontools/iterator/StrideIterator.hpp"
#include <typeinfo>


namespace
{

   class IteratorTest : public ::testing::Test
   {
   };

   TEST_F(IteratorTest, pointer) // NOLINT
   {
      size_t increment(5);

      recontools::iterator::StrideIterator<double *>::value_type data[increment * 2];

      data[0] = 0.0;
      data[increment] = 1.0;

      // This make sure all parts of StrideIterator are used
      recontools::iterator::StrideIterator iter(data, increment);

      EXPECT_EQ(*iter, data[0]);
      EXPECT_EQ(*++iter, data[increment]);
   }

   TEST_F(IteratorTest, vector) // NOLINT
   {
      size_t increment(5);
      std::vector<double> data(increment * 2, 0.0);

      data[increment] = 1.0;

      // This make sure all parts of StrideIterator are used
      recontools::iterator::StrideIterator iter(data.begin(), increment);

      EXPECT_EQ(*iter, data[0]);
      EXPECT_EQ(*++iter, data[increment]);
   }

   TEST_F(IteratorTest, type) // NOLINT
   {
      EXPECT_TRUE(typeid(recontools::iterator::StrideIterator<double *>::iterator_category) == typeid(std::random_access_iterator_tag));
   }

}