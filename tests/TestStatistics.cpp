//
// Created by Martyn Klassen on 2019-12-06.
//
#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include "recontools/statistics/peirce.hpp"
#include "recontools/statistics/students_t.hpp"
#include <boost/math/distributions/students_t.hpp>
#include "recontools/statistics/ms_shapiro_wilk.hpp"
#include <random>

namespace
{

   class StatisticsTest : public ::testing::Test
   {
   };

   TEST_F(StatisticsTest, peirce) // NOLINT
   {
      // A sampling of test values from
      // Gould, B.A., "On Peirce's criterion for the rejection of doubtful observations, with tables for facilitating its application", Astronomical Journal, issue 83, vol. 4, no. 11, pp. 81–87, 1855
      EXPECT_NEAR(recontools::statistics::peirce_dev(3, 1, 1), 1.480, 1e-3);
      EXPECT_NEAR(recontools::statistics::peirce_dev(10, 2, 1), 2.464, 1e-3);
      EXPECT_NEAR(recontools::statistics::peirce_dev(10, 2, 2), 2.277, 1e-3);
      EXPECT_NEAR(recontools::statistics::peirce_dev(40, 1, 2), 6.145, 1e-3);
      EXPECT_NEAR(recontools::statistics::peirce_dev(28, 9, 2), 1.848, 1e-3);

      // A value as computed
      EXPECT_NEAR(recontools::statistics::peirce_dev(5, 1, 1), 2.2779145245929358, 1e-15);
   }

   TEST_F(StatisticsTest, students_t)
   {
      double t(-2.132);
      size_t n(5);

      using namespace boost::math;
      students_t dist(n - 1);
      double result = cdf(complement(dist, fabs(t)));

      EXPECT_NEAR(recontools::statistics::run_students_t(n, t), result, 1e-15);
   }


   TEST_F(StatisticsTest, shapiro_wilk)
   {
      recontools::statistics::matrix_science::ms_shapiro_wilk sw;
      std::vector<float> swValues;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      std::normal_distribution<> d{5.0, 2.0};

      swValues.reserve(100);
      for (auto i = 0; i < 100; i++)
      {
         swValues.push_back(static_cast<float>(d(gen)));
      }

      std::sort(swValues.begin(), swValues.end());

      for (float swValue : swValues)
      {
         sw.appendSampleValue(swValue);
      }

      sw.calculate(swValues.size(), swValues.size(), swValues.size() / 2);
      EXPECT_LE(sw.getErrorCode(), 0.0);
      // Distribution is normal so probability should always be high
      EXPECT_GT(sw.getPValue(), 0.01) << "Normalize distribution rejected as normal at " << sw.getPValue();


      std::uniform_real_distribution<> d2{1.0, 10.0};

      swValues.clear();
      for (int i = 0; i < 100; i++)
      {
         swValues.push_back(static_cast<float>(d2(gen)));
      }

      std::sort(swValues.begin(), swValues.end());
      sw.clearSampleValues();
      for (float swValue : swValues)
      {
         sw.appendSampleValue(swValue);
      }

      sw.calculate(swValues.size(), swValues.size(), swValues.size() / 2);
      EXPECT_LE(sw.getErrorCode(), 0.0);
      // Distribution is not normal so probability should be low
      EXPECT_LT(sw.getPValue(), 0.01) << " Uniform distribution not rejected as normal at " << sw.getPValue();
   }

}
