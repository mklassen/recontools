//
// Created by Martyn Klassen on 2019-12-11.
//

#include "gmock/gmock.h"

#pragma clang diagnostic push
#pragma ide diagnostic ignored "CannotResolve"

#include <recontools/validation/B1Validator.hpp>
#include <vector>
#include <complex>
#include <random>


namespace
{

   class ValidationTest : public ::testing::Test
   {
   };

   TEST_F(ValidationTest, cutoff) // NOLINT
   {

      recontools::validation::B1Validator<std::vector<std::complex<double>>::iterator, double> oValidator;

      double cutoff(10.0);
      double alpha(2.0);
      size_t images(0);
      double alphaSW(2.0);

      oValidator.setStatistics(cutoff, alpha, images, alphaSW);

      std::vector<std::complex<double>> data;

      EXPECT_TRUE(oValidator.isValid(cutoff * 2.0, data.begin(), data.end()));
      EXPECT_FALSE(oValidator.isValid(cutoff * 0.5, data.begin(), data.end()));

   }

   TEST_F(ValidationTest, zero) // NOLINT
   {
      recontools::validation::B1Validator<std::vector<std::complex<double>>::iterator, double> oValidator;

      double cutoff(10.0);
      double alpha(0.025);
      size_t images(0);
      double alphaSW(2.0);

      oValidator.setStatistics(cutoff, alpha, images, alphaSW);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      // values near the mean are the most likely
      // standard deviation affects the dispersion of generated values from the mean
      std::normal_distribution<> d{0.0, 1.0};

      data.reserve(10);
      for (auto i = 0; i < data.capacity(); i++)
      {
         data.emplace_back(d(gen), d(gen));
      }

      int count(0);
      int validCount(0);
      while (count < 1000)
      {
         for (auto i = 0; i < data.capacity(); i++)
         {
            data[i] = std::complex<double>(d(gen), d(gen));
         }
         if (oValidator.isValid(cutoff * 0.5, data.begin(), data.end()))
            validCount++;
         count++;
      }

      // 10% failure rate is expected
      // alpha = 0.05 for real and imaginary, i.e. 5% fail on real and 5% fail on imaginary
      // Set failure cutoff at 12%
      EXPECT_LE(validCount, 120) << validCount << " of " << count << " zero mean distributions marked as valid";
   }

   TEST_F(ValidationTest, nonzero) // NOLINT
   {
      recontools::validation::B1Validator<std::vector<std::complex<double>>::iterator, double> oValidator;

      double cutoff(10.0);
      double alpha(0.05);
      size_t images(0);
      double alphaSW(2.0);

      oValidator.setStatistics(cutoff, alpha, images, alphaSW);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      std::normal_distribution<> d{10.0, 2.0};

      data.reserve(10);
      for (auto i = 0; i < data.capacity(); i++)
      {
         data.emplace_back(d(gen), d(gen));
      }

      EXPECT_TRUE(oValidator.isValid(cutoff * 0.5, data.begin(), data.end()));
   }

   TEST_F(ValidationTest, outlier) // NOLINT
   {
      recontools::validation::B1Validator<std::vector<std::complex<double>>::iterator, double> oValidator;

      double cutoff(10.0);
      double alpha(2.0);
      size_t images(10);
      double alphaSW(2.0);

      oValidator.setStatistics(cutoff, alpha, images, alphaSW);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      // values near the mean are the most likely
      // standard deviation affects the dispersion of generated values from the mean
      std::normal_distribution<> d{10.0, 3.0};

      data.reserve(images);
      for (auto i = 0; i < data.capacity(); i++)
      {
         data.emplace_back(d(gen), d(gen));
      }

      data[0].real(30.0);

      EXPECT_TRUE(oValidator.isValid(cutoff * 0.5, data.begin(), data.end()));
   }

   TEST_F(ValidationTest, normal) // NOLINT
   {
      recontools::validation::B1Validator<std::vector<std::complex<double>>::iterator, double> oValidator;

      double cutoff(10.0);
      double alpha(2.0);
      size_t images(0);
      double alphaSW(0.05);

      oValidator.setStatistics(cutoff, alpha, images, alphaSW);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      // values near the mean are the most likely
      // standard deviation affects the dispersion of generated values from the mean
      std::normal_distribution<> d{10.0, 3.0};

      data.reserve(10);
      for (auto i = 0; i < data.capacity(); i++)
      {
         data.emplace_back(d(gen), d(gen));
      }

      int count(0);
      int validCount(0);
      while (count < 1000)
      {
         for (auto i = 0; i < data.capacity(); i++)
         {
            data[i] = std::complex<double>(d(gen), d(gen));
         }
         if (oValidator.isValid(cutoff * 0.5, data.begin(), data.end()))
            validCount++;
         count++;
      }

      // 10% failure rate is expected
      // alpha = 0.05 for real and imaginary, i.e. 5% fail on real and 5% fail on imaginary
      // Set failure cutoff at 12%
      EXPECT_LE(validCount, 120) << validCount << " of " << count << " zero mean distributions marked as valid";
   }

   TEST_F(ValidationTest, nonnormal) // NOLINT
   {
      recontools::validation::B1Validator<std::vector<std::complex<double>>::iterator, double> oValidator;

      double cutoff(10.0);
      double alpha(2.0);
      size_t images(0);
      double alphaSW(0.05);

      oValidator.setStatistics(cutoff, alpha, images, alphaSW);

      std::vector<std::complex<double>> data;

      std::random_device rd{};
      std::mt19937 gen{rd()};

      std::uniform_real_distribution<> d{1.0, 10.0};

      data.reserve(100);
      for (auto i = 0; i < data.capacity(); i++)
      {
         data.emplace_back(d(gen), d(gen));
      }

      EXPECT_TRUE(oValidator.isValid(cutoff * 0.5, data.begin(), data.end()));
   }
}