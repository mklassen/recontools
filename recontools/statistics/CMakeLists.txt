set(RECONTOOLS_SRC_FILES ${RECONTOOLS_SRC_FILES}
      ${CMAKE_CURRENT_SOURCE_DIR}/ms_shapiro_wilk.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/peirce.cpp
      ${CMAKE_CURRENT_SOURCE_DIR}/students_t.cpp
      PARENT_SCOPE)
set(RECONTOOLS_HDR_FILES ${RECONTOOLS_HDR_FILES}
      ${CMAKE_CURRENT_SOURCE_DIR}/ms_shapiro_wilk.hpp
      ${CMAKE_CURRENT_SOURCE_DIR}/peirce.hpp
      ${CMAKE_CURRENT_SOURCE_DIR}/students_t.hpp
      PARENT_SCOPE)

install(FILES
      ms_shapiro_wilk.hpp
      peirce.hpp
      students_t.hpp
      DESTINATION ${RECONTOOLS_INSTALL_INCLUDE_PATH}/statistics COMPONENT main)
