//
// Created by Martyn Klassen on 2019-12-04.
//

#define _USE_MATH_DEFINES
#include <cmath>
#ifdef BOOST
#include <boost/math/distributions/students_t.hpp>
#else
#include "students_t.hpp"
#endif


namespace recontools
{
   namespace statistics
   {
#ifdef BOOST
      RECONTOOLS_API double run_students_t(int n, double t)
      {
         using namespace boost::math;
         students_t dist(n-1);
         return cdf(complement(dist, fabs(t)));
      }
#else
      RECONTOOLS_API double run_students_t(int k, double x)
      {
         double rk;
         double z;
         double f;
         double tz;
         double p;
         double x_sq_k;
         int j;

         if (x == 0.0) return 0.5;

         // Remove degree of freedom
         k--;

         // t-test is symmetric, so always use positive value
         x = fabs(x);

         rk = (double) (k);
         z = 1.0 + x * x / rk;
         if (k % 2 != 0)
         {
            x_sq_k = x / sqrt(rk);
            p = atan(x_sq_k);
            if (k > 1)
            {
               f = 1.0;
               tz = 1.0;
               j = 3;
               while (j <= k - 2 && (tz / f > 5e-16))
               {
                  tz = tz * ((j - 1) / (z * j));
                  f = f + tz;
                  j = j + 2;
               }
               p = p + f * x_sq_k / z;
            }
            p = p * 2.0 / M_PI;
         }
         else
         {
            f = 1.0;
            tz = 1.0;
            j = 2;
            while (j <= k - 2 && (tz / f > 5e-16))
            {
               tz = tz * ((j - 1) / (z * j));
               f = f + tz;
               j = j + 2;
            }
            p = f * x / sqrt(z * rk);
         }
         return 0.5 - 0.5 * p;
      }
#endif
   }
}
