//
// Created by Martyn Klassen on 2019-12-03.
//
#include <cmath>
#include "peirce.hpp"

namespace recontools
{
   namespace statistics
   {

      double erfc(double x)
      {
         // constants
         double a1 = 0.254829592;
         double a2 = -0.284496736;
         double a3 = 1.421413741;
         double a4 = -1.453152027;
         double a5 = 1.061405429;
         double p = 0.3275911;

         // Save the sign of x
         int sign = 1;
         if (x < 0)
            sign = -1;
         x = fabs(x);

         // A&S formula 7.1.26
         double t = 1.0 / (1.0 + p * x);
         double y = 1.0 - (((((a5 * t + a4) * t) + a3) * t + a2) * t + a1) * t * exp(-x * x);

         return 1.0 - sign * y;
      }

      RECONTOOLS_API double peirce_dev(double N, double n, double m)
      {
         /*
         Name:     peirce_dev
         Input:    - int, total number of observations (N)
                   - int, number of outliers to be removed (n)
                   - int, number of model unknowns (m)
         Output:   float, squared error threshold (x2)
         Features: Returns the squared threshold error deviation for outlier
                   identification using Peirce's criterion based on Gould's
                   methodology
         */
         double x2 = 0.0;
         // Check number of observations
         if (N > 1.0)
         {
            // Calculate Q (Nth root of Gould's equation B):
            double Q = pow(n, n / N) * pow(N - n, (N - n) / N) / N;

            // Initialize R values (as floats)
            double dRNew = 1.0;
            double dROld = 0.0;

            // Start iteration to converge on R:
            while (fabs(dRNew - dROld) > (N * 2.0e-16))
            {
               // Calculate lamda
               // (1/(N-n)th root of Gould's equation A'):
               double ldiv = pow(dRNew, n);
               if (ldiv == 0)
                  ldiv = 1.0e-6;
               double dLamda = pow(pow(Q, N) / ldiv, 1.0 / (N - n));

               // Calculate x-squared (Gould's equation C):
               x2 = 1.0 + (N - m - n) / n * (1.0 - dLamda * dLamda);

               // If x2 goes negative, return 0:
               if (x2 < 0)
               {
                  x2 = 0.0;
                  dROld = dRNew;
               }
               else
               {
                  // Use x-squared to update R (Gould's equation D):
                  dROld = dRNew;
                  dRNew = exp((x2 - 1) / 2.0) * erfc(sqrt(x2 / 2.0));
               }
            }
         }
         return x2;
      }

   }
}
