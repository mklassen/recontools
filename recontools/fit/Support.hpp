//
//  Support.h
//  IceSvd
//
//  Created by Martyn Klassen on 2016-04-18.
//  Copyright © 2016 CFMM. All rights reserved.
//
#ifndef RECONTOOLS_SUPPORT_HPP
#define RECONTOOLS_SUPPORT_HPP

#include <vector>
#include <cmath>
#include <ostream>
#include <cstring>
#include "recontools/config.hpp"

namespace recontools
{
   namespace fit
   {

      struct RECONTOOLS_API P3
      {
         typedef double value_type;

         value_type position[3];

         P3()
         {
            std::memset(position, 0, sizeof(*position) * 3);
         }

         P3(const value_type &R, const value_type &C, const value_type &Z)
         {
            position[0] = R;
            position[1] = C;
            position[2] = Z;
         }

         P3(const P3 &p)
         {
            std::memcpy(position, p.position, sizeof(*position) * 3);
         }

         explicit P3(const value_type *p)
         {
            std::memcpy(position, p, sizeof(*position) * 3);
         }

         value_type len2()
         {
            return position[0] * position[0] + position[1] * position[1] + position[2] * position[2];
         }

         value_type len()
         {
            return sqrt(len2());
         }

         void assign(value_type *pos)
         {
            pos[0] = position[0];
            pos[1] = position[1];
            pos[2] = position[2];
         }

         P3 &operator=(const P3 &p)
         {
            position[0] = p.position[0];
            position[1] = p.position[1];
            position[2] = p.position[2];
            return *this;
         }

         P3 &operator*=(const value_type &v)
         {
            position[0] *= v;
            position[1] *= v;
            position[2] *= v;
            return *this;
         }

         P3 &operator/=(const value_type &v)
         {
            position[0] /= v;
            position[1] /= v;
            position[2] /= v;
            return *this;
         }

         P3 &operator+=(const P3 &p)
         {
            position[0] += p.position[0];
            position[1] += p.position[1];
            position[2] += p.position[2];
            return *this;
         }

         P3 &operator-=(const P3 &p)
         {
            position[0] -= p.position[0];
            position[1] -= p.position[1];
            position[2] -= p.position[2];
            return *this;
         }

         bool operator==(const value_type &v) const
         {
            return (position[0] == v) && (position[1] == v) && (position[2] == v);
         }

         bool operator==(const P3 &rhs) const
         {
            return (position[0] == rhs.position[0]) && (position[1] == rhs.position[1]) && (position[2] == rhs.position[2]);
         }

         bool operator!=(const P3 &rhs) const
         {
            return (position[0] != rhs.position[0]) || (position[1] != rhs.position[1]) || (position[2] != rhs.position[2]);
         }

         friend P3 operator+(const P3 &lhs, const P3 &rhs)
         {
            return P3(lhs.position[0] + rhs.position[0], lhs.position[1] + rhs.position[1], lhs.position[2] + rhs.position[2]);
         }

         friend P3 operator-(const P3 &lhs, const P3 &rhs)
         {
            return P3(lhs.position[0] - rhs.position[0], lhs.position[1] - rhs.position[1], lhs.position[2] - rhs.position[2]);
         }

         friend value_type operator*(const P3 &p1, const P3 &p2)
         {
            return p1.position[0] * p2.position[0] + p1.position[1] * p2.position[1] + p1.position[2] * p2.position[2];
         }

         friend P3 operator*(const P3 &lhs, const value_type &v)
         {
            return P3(lhs.position[0] * v, lhs.position[1] * v, lhs.position[2] * v);
         }

         friend P3 operator*(const value_type &v, const P3 &rhs)
         {
            return P3(rhs.position[0] * v, rhs.position[1] * v, rhs.position[2] * v);
         }

         friend P3 cross(const P3 &p1, const P3 &p2)
         {
            return P3(p1.position[1] * p2.position[2] - p1.position[2] * p2.position[1],
                      p1.position[2] * p2.position[0] - p1.position[0] * p2.position[2],
                      p1.position[0] * p2.position[1] - p1.position[1] * p2.position[0]);
         }

         friend bool operator<(const P3 &a, const P3 &b)
         {
            if (a.position[2] == b.position[2])
            {
               if (a.position[0] == b.position[0])
               {
                  return a.position[1] < b.position[1];
               }
               return a.position[0] < b.position[0];
            }
            return a.position[2] < b.position[2];
         }

         friend std::ostream &operator<<(std::ostream &stream, const P3 &a)
         {
            return stream << "(" << a.position[0] << ", " << a.position[1] << ", " << a.position[2] << ")";
         }
      };


      class RECONTOOLS_API Support
      {
      public:
         typedef P3::value_type value_type;
         typedef std::vector<bool> mask_type;
         typedef std::vector<value_type> data_type;
         typedef size_t size_type;
         typedef std::vector<size_type> index_type;

         Support(size_type dims = 3)
         {
            dimensions(dims);
         }


         virtual ~Support()
         {}

         virtual void computePerimeter(const mask_type &mask, std::vector<size_type> const &dims, const value_type *center, const value_type *voxel) = 0;

         virtual bool interior(value_type *pos) = 0;

         const data_type &center() const
         { return m_vOffset; }

         virtual bool write(std::ostream &stream);

         virtual bool read(std::istream &stream);

         bool operator==(Support const &rhs)
         {
            return this->dimensions() == rhs.dimensions();
         }

      protected:
         virtual void setupWork(size_type numPoints) = 0;

         virtual void setPosition(value_type *offset) = 0;

         size_type configure(const mask_type &mask, std::vector<size_type> const &dims, const value_type *center, const value_type *voxel);

         static void removeColinear(mask_type::iterator const &vertices, size_type d, const size_type *dimVertex, const size_type *incrVertex);

         static void match(mask_type::iterator vertexMask, mask_type::const_iterator iterVoxel, std::vector<size_type>::const_reverse_iterator voxelDim, const size_type *voxelIncr, const size_type *vertexIncr);

         static void colinear(mask_type::iterator const &mask, const size_type &dim, const size_type &incr);

         static void colinearLoop(mask_type::iterator mask, const size_type &apply, int current, const size_type *dim, const size_type *incr);

         void calculatePosition(value_type *position, const value_type *offset, value_type factor = 1.0) const;

         void populate(mask_type::iterator mask, size_type *dim, size_type *incr, value_type *offset);

         size_type dimensions() const
         { return m_vOffset.size(); }

         void dimensions(size_type dims);

      private:
         data_type m_vVoxel;
         data_type m_vOffset;
      };
   }
}

#endif //RECONTOOLS_SUPPORT_HPP
