//
// Created by Martyn Klassen on 2019-12-06.
//

#ifndef RECONTOOLS_B1MAP_HPP
#define RECONTOOLS_B1MAP_HPP

#include <complex>
#include <sstream>
#include <recontools/fit/ConvexHull.hpp>
#include <recontools/fit/SolidHarmonics.hpp>
#include <recontools/iterator/StrideIterator.hpp>
#include <cpplapack/cpplapack.h>
#include <cstring>
#include <algorithm>

namespace recontools
{
   namespace fit
   {

      template<typename basis_type=SolidHarmonic<double>, class V=ConvexHull>
      class B1Fit
      {
      public:
         typedef V validity_type;
         typedef typename basis_type::value_type precision_type;
         typedef typename std::complex<precision_type> value_type;
         typedef typename std::vector<value_type> data_type;

         explicit B1Fit(size_t order = 5, typename basis_type::result_type arg = typename basis_type::result_type(0.0))
               : m_oBasis(order, arg)
               , m_iCoils(0)
         {}

         virtual ~B1Fit()
         {}

         bool operator==(B1Fit<basis_type, validity_type> const &rhs)
         {
            return m_oBasis == rhs.m_oBasis &&
                   m_oValidity == rhs.m_oValidity &&
                   m_vCoefficients == rhs.m_vCoefficients &&
                   m_iCoils == rhs.m_iCoils;
         }

         void coefficients(size_t coils, data_type const &coefficient, size_t lda = 0)
         {
            coefficients(coils, &(coefficient.front()), lda);
         }

         void coefficients(size_t coils, value_type const *coefficient, size_t lda = 0)
         {
            m_iCoils = coils;
            size_t nBasis = this->setup();
            if (!lda) lda = nBasis;

            value_type const *pInput = coefficient;
            value_type *pOutput = &(m_vCoefficients.front());

            for (size_t i = 0; i < coils; i++)
            {
               std::memcpy(pOutput, pInput, sizeof(*pInput) * nBasis);
               pInput += lda;
               pOutput += nBasis;
            }
         }

         size_t coils() const
         { return m_iCoils; }

         const value_type *coefficients() const
         { return &(m_vCoefficients.front()); }

         bool fit(size_t iterations);

         void compute(P3 const &pos, typename basis_type::result_type *data)
         {
            m_oBasis.compute(pos.position, data);
         }

         const data_type &data(P3 &pos)
         {
            // Move to closest location within the support
            m_oValidity.closest(pos);

            // Get the value of the basis at the location
            m_oBasis.compute(pos.position, m_vBasis.begin());

            // Compute the matrix-vector multiplication
            // Coefficients^T * basis = coil maps
            // (bBasis x nCoils)^T * (nBasis x 1) = (nCoils x 1)
            // (nCoils x nBasis) * (nBasis x 1) = (nCoils x 1)
            size_t nBasis = m_vBasis.size();
            cpplapack::gemv('T', nBasis, m_iCoils, 1.0, m_vCoefficients, nBasis, m_vBasis, 1ul, 0.0, m_vData, 1ul);

            return m_vData;
         }

         const basis_type &basis() const
         { return m_oBasis; }

         const validity_type &validity() const
         { return m_oValidity; }


         virtual bool write(std::ostream &stream)
         {
            if (!m_oBasis.write(stream) || !m_oValidity.write(stream))
            {
               return false;
            }
            stream.write(reinterpret_cast<const char *>(&m_iCoils), sizeof(m_iCoils));
            stream.write(reinterpret_cast<const char *>(&(m_vCoefficients.front())), m_vCoefficients.size() * sizeof(value_type));
            return true;
         }

         virtual bool read(std::istream &stream)
         {
            if (!m_oBasis.read(stream) || !m_oValidity.read(stream))
            {
               return false;
            }
            stream.read(reinterpret_cast<char *>(&m_iCoils), sizeof(m_iCoils));
            this->setup();
            stream.read(reinterpret_cast<char *>(&(m_vCoefficients.front())), m_vCoefficients.size() * sizeof(value_type));
            return true;
         }

      protected:
         virtual bool mask(typename validity_type::mask_type const &mask, std::vector<size_t> const &dims,
               typename validity_type::value_type const *center, typename validity_type::value_type const *voxel)
         {
            m_oValidity.computePerimeter(mask, dims, center, voxel);
            return true;
         }

         virtual void populate() =0;

         void allocate()
         {
            // Compute the optimum workspace size
            size_t lWork = -1;
            value_type dWork;
            size_t nBasis = m_oBasis.terms();

            // Locally 3 * nBasis workspace is required for wmin and wmax during rank testing
            int i = 3 * nBasis;

#ifndef BUILD_PLATFORM_LINUX
            // Query all the LAPACK calls to find the optimum workspace
            // The largest optimum workspace will be used
            cpplapack::unmqr('L', 'C', m_lPoints, m_iCoils, nBasis, NULL, m_lPoints, NULL, NULL, m_lPoints, &dWork, lWork);
            i = std::max(i, static_cast<int>(dWork.real()));

            cpplapack::unmqr('L', 'N', m_lPoints, m_iCoils, nBasis, NULL, m_lPoints, NULL, NULL, m_lPoints, &dWork, lWork);
            i = std::max(i, static_cast<int>(dWork.real()));

            cpplapack::geqp3(m_lPoints, nBasis, NULL, m_lPoints, NULL, NULL, &dWork, lWork, NULL);
            i = std::max(i, static_cast<int>(dWork.real()));

            cpplapack::tzrzf(nBasis, nBasis, NULL, m_lPoints, NULL, &dWork, lWork);
            lWork = std::max(i, static_cast<int>(dWork.real()));
#else
            //LAPACK does not succeed it's workspace queries on the scanner. There is a memory error.
            // Since we do not have control over MKL on MRIR is it easier to do the initial calculations ourselves
            //unmqr needs max(1,N) or max(1,m_iCoils) this is what it reurns on a workspace query so it is fine to use that
            lWork = std::max(static_cast<size_t>(i), m_iCoils);
            //geqp3 needs (N+1)*NB which is block size, we don't know blocksize BUT we know in windows it returns nBasis+1
            //tzrzf needs M*NB but blocksize is unknown so we will just go with nBasis since it seems NB is 1 from geqp3
            //both of these cases are accounted in the initial guess of 3*nBasis
#endif

            // Allocate complex memory
            m_vB_1.resize(m_lPoints * m_iCoils);
            m_vB_2.resize(m_lPoints * m_iCoils);
            m_vA.resize(m_lPoints * nBasis);
            m_vD.resize(m_lPoints);
            m_vWork.resize(lWork);
            m_vTau.resize(nBasis);
            m_vTauZ.resize(nBasis);

            m_vW.resize(m_lPoints);
            m_vrWork.resize(2 * nBasis);

            // Pivoting memory must be initialized to zero for it to work correctly
            m_vJPVT.assign(nBasis, 0);
         }

         size_t m_lPoints;

         data_type m_vB_1;
         data_type m_vA;
         data_type m_vWork;
         std::vector<precision_type> m_vW;

      private:
         size_t setup()
         {
            size_t nBasis = m_oBasis.terms();
            m_vCoefficients.resize(m_iCoils * nBasis);
            m_vData.resize(m_iCoils);
            m_vBasis.resize(nBasis);
            return nBasis;
         }

         data_type m_vData;
         data_type m_vBasis;

         basis_type m_oBasis;
         validity_type m_oValidity;

         size_t m_iCoils;
         data_type m_vCoefficients;

         data_type m_vB_2;
         data_type m_vD;
         data_type m_vTau;
         data_type m_vTauZ;

         std::vector<precision_type> m_vrWork;
         std::vector<int> m_vJPVT;

      };


      template<class basis_type, class V>
      bool B1Fit<basis_type, V>::fit(size_t iterations)
      {
         allocate();
         populate();

         size_t nBasis = m_oBasis.terms();

         try
         {
            // Solve the basis fitting using QR with pivoting
            // This algorithm is derived from LAPACK zgelsy function
            // Because we must solve the problem iteratively, zgelsy is
            // not directly applicable as it would require significant
            // unnecessary duplication during each iteration

            // Iteratively find the best fit for the B1 maps to the basis
            // with an additional spatially varying scaling term that is the same for all
            // coils

            // The B maps have l-2 norm = 1 at every point
            // The scaling from unity is the weighting (common term) of the fit, therefore
            // the generated maps should be around 1, i.e. they should not significantly
            // affect scaling of the output when used to remove coil sensitivity for detected
            // signal, i.e magnetization

            // Get machine precision information
            precision_type smallNumber;
            precision_type bigNumber;
            cpplapack::lamch('S', smallNumber);
            cpplapack::lamch('P', bigNumber);
            smallNumber /= bigNumber;
            bigNumber = 1.0 / smallNumber;
            cpplapack::labad(smallNumber, bigNumber);
            int Ascale(0);
            int Bscale(0);

            // Apply scaling if the maximum entries are outside [SMLNUM, BIGNUM]
            precision_type scaleA = cpplapack::lange('M', m_lPoints, nBasis, m_vA, m_lPoints, m_vrWork);
            if (scaleA > 0 && scaleA < smallNumber)
            {
               // Scale matrix norm up to smallNumber
               cpplapack::lascl('G', 0l, 0l, scaleA, smallNumber, m_lPoints, nBasis, m_vA, m_lPoints);
               Ascale = 1;
            }
            else if (scaleA > bigNumber)
            {
               // Scale matrix norm down to bigNumber
               cpplapack::lascl('G', 0l, 0l, scaleA, bigNumber, m_lPoints, nBasis, m_vA, m_lPoints);
               Ascale = 2;
            }

            precision_type scaleB = cpplapack::lange('M', m_lPoints, m_iCoils, m_vB_1, m_lPoints, m_vrWork);
            if (scaleB > 0 && scaleB < smallNumber)
            {
               cpplapack::lascl('G', 0l, 0l, scaleB, smallNumber, m_lPoints, m_iCoils, m_vB_1, m_lPoints);
               Bscale = 1;
            }
            else if (scaleB > bigNumber)
            {
               cpplapack::lascl('G', 0l, 0l, scaleB, bigNumber, m_lPoints, m_iCoils, m_vB_1, m_lPoints);
               Bscale = 2;
            }

            // Perform the QR decomposition of the basis and store the results into A
            // A * P = (Q1 Q2) * (R; 0)
            cpplapack::geqp3(m_lPoints, nBasis, m_vA, m_lPoints, m_vJPVT, m_vTau, m_vWork, m_vWork.size(), m_vrWork);

            // Find the rank of A
            if (m_vA.front() == 0.0)
            {
               // Basis is rank 0
               return false;
            }

            value_type *wmin = &(m_vWork.front());
            value_type *wmax = wmin + 2 * nBasis;
            *wmin = 1.0;
            *wmax = 1.0;

            precision_type smax = std::abs(m_vA.front());
            precision_type smin = smax;
            value_type s1, s2, c1, c2;
            precision_type sminpr, smaxpr;
            // Recommended rcond = sqrt(epsilon/2);
            // NAG F08BAF (DGELSY) documentation
            precision_type rcond = sqrt(std::numeric_limits<precision_type>::epsilon() / 2.0);

            size_t rank = 1;
            typename data_type::iterator iterA = m_vA.begin();
            while (rank < nBasis)
            {
               iterA += m_lPoints;
               cpplapack::laic1(2, rank, wmin, smin, &(*iterA), iterA[rank], sminpr, s1, c1);
               cpplapack::laic1(1, rank, wmax, smax, &(*iterA), iterA[rank], smaxpr, s2, c2);

               if (smaxpr * rcond <= sminpr)
               {
                  for (size_t i = 0; i < rank; i++)
                  {
                     wmin[i] *= s1;
                     wmax[i] *= s2;
                  }
                  wmin[rank] = c1;
                  wmax[rank] = c2;
                  smin = sminpr;
                  smax = smaxpr;
                  rank++;
               }
               else
                  break;
            }

            // Error of ill-conditioning
            if (rank < nBasis)
            {
               return false;
            }

            // b is the sensitivity estimate with an unknown global, i.e. all channels, term
            // B is the sensitivity estimate without the global term

            // Copy b to B for first iteration, i.e. assume global term is unity
            m_vB_2 = m_vB_1;

            // A and tau contain Q and R
            // R is the upper triangular part of A
            // Q can be derived from tau and lower part of A
            // A = Q * R = (Q1 Q2) * (R; 0) = Q1 * R

            // R may be rank deficient. The rank has been estimated so the relevant
            // size of Q1 and Q2 is know. It is not yet necessary to compute
            // (R; 0) = (R11 R12; 0 R22) = (T11 0; 0 0) * Z
            // because only Q and Q1 are required during iterations.

            // Solve for Q^H * B
            // Results stored in B
            cpplapack::unmqr('L', 'C', m_lPoints, m_iCoils, nBasis, m_vA, m_lPoints, m_vTau, m_vB_2, m_lPoints, m_vWork, m_vWork.size());

            // l-2 norm of the global term should be sqrt(points) if global term is unity
            // Since we want the global to term be unity on average, we want to target an
            // l-2 norm of sqrt(points)
            precision_type rPoints = sqrt(1.0 / static_cast<double>(m_lPoints));

            // Setup convergence monitoring terms
            precision_type residual;
            precision_type residual_prev = std::numeric_limits<double>::max();

            typename data_type::iterator iterB_2 = m_vB_2.begin();
            typename data_type::iterator iterB_1 = m_vB_1.begin();

            precision_type scale;
            value_type factor;
            size_t j;
            for (size_t i = 0; i < iterations; i++)
            {
               // Compute the residual of the fit
               // The first rank term of Q^H * B are used for fitting and the remaining (points-rank)
               // terms reflect the residual of the fit.
               // Compute the l2-norm of all residual terms for all coils combined.
               // Residual terms are nulled after summing to permit computing Q1 * Q1^H * B = A * P * x
               // r = b - A * x = Q * (0; Q2^H * b);
               // |r|_2 =  (Q^H * b)^H * Q^H * Q * (Q^H * b) = (Q^H * b)^H * (Q^H * b) = | Q2^H * b |_2
               typename data_type::iterator iterB = m_vB_2.begin();
               residual = 0.0;
               while (iterB != m_vB_2.end())
               {
                  iterB += rank;
                  for (j = rank; j < m_lPoints; j++)
                  {
                     residual += std::norm(*iterB);
                     *iterB = 0.0;
                     iterB++;
                  }
               }

               // Stop if the total residual decreases by less than 0.01%
               // TODO Make a better cutoff
               if (residual > 0.9999 * residual_prev)
                  break;
               residual_prev = residual;

               // Solve for M = Q1 * Q1^H * b = Q1 * R * x = A * P * x
               // On input B stores Q1^H * b
               // The first rank rows hold Q1^H * b
               // You might think that you can change the first "points" to "rank" in this call
               // but that does not work. Therefore the Q2^H * b portions was zeroed out
               // during residual calculation and the full multiplication is done.
               // This gives the correct results.
               // Results stored in B
               cpplapack::unmqr('L', 'N', m_lPoints, m_iCoils, nBasis, m_vA, m_lPoints, m_vTau, m_vB_2, m_lPoints, m_vWork, m_vWork.size());

               // Find the best scaling factor to make the data match the fit
               scale = 0.0;

               typename data_type::iterator iterD = m_vD.begin();
               typename std::vector<precision_type>::iterator iterW = m_vW.begin();

               iterB_2 = m_vB_2.begin();
               iterB_1 = m_vB_1.begin();

               precision_type weight;
               while (iterD != m_vD.end())
               {

                  // Weighted Mean of Mi ./ bi for all coils
                  // Mi ./ bi = Mi * bi^H / (bi * bi^H)
                  // Weighting should be by bi * bi^H
                  // So the weighted term to sum is Mi * bi^H
                  // Note that M is stored in B and both M and b have total fit weighting applied,
                  // but the ratio will be independent of this weighting
                  // W stores 1 / sqrt(sum(bi*bi^H))
                  // The weighted sum of Mi * bi^H must be multiplied by W^2 to get the weighted mean
                  // The m_iCoils term is ignored as it is constant and will drop out in later scaling
                  // W is applied in two parts to minimize potential overflow issues
                  weight = *iterW;
                  factor = 0.0;
                  recontools::iterator::StrideIterator<typename data_type::iterator> iterB_2b(iterB_2, m_lPoints);
                  recontools::iterator::StrideIterator<typename data_type::iterator> iterB_1b(iterB_1, m_lPoints);
                  recontools::iterator::StrideIterator<typename data_type::iterator> iterB_1b_end(iterB_1b);
                  iterB_1b += m_iCoils;
                  while (iterB_1b != iterB_1b_end)
                  {
                     factor += *iterB_2b++ * std::conj(*iterB_1b++) * weight;
                  }

                  *iterD = factor * weight;

                  // Compute the l2-norm of d
                  // Scaled by rPoints = 1/sqrt(points)
                  // The other half of the scaling is applied later
                  // to provide better overflow behaviour
                  scale += std::norm(*iterD) * rPoints;

                  // Move to the next voxel
                  iterD++;
                  iterW++;
                  iterB_1++;
                  iterB_2++;
               }

               // Multiply l2-norm of d by second 1/sqrt(points)
               // in total the scaling is now 1/points
               // scale = (d^H * d) / points
               scale *= rPoints;

               // Sanity check that should never occur
               if (scale < 100.0 * DBL_EPSILON)
               {
                  return false;
               }

               // Normalize d to mean magnitude of 1.0 or l2-norm / sqrt(points) = sqrt(d * d^H) / sqrt(points) == 1.0
               // scale is current (d^H * d) / points
               // scale = sqrt(1/scale) = sqrt(points) / sqrt(d^H * d) = sqrt(points) / l2-norm
               scale = sqrt(1.0 / scale);
               iterD = m_vD.begin();
               while (iterD != m_vD.end())
               {
                  *iterD++ *= scale;
               }

               // Create the scaled mapping for the next iteration
               // This might influence the optimum scaling of b, but because
               // l2-norm of d is sqrt(points), i.e average value of d is one,
               // the norm of B should on average be close to the optimum unity.
               iterB_1 = m_vB_1.begin();
               iterB_2 = m_vB_2.begin();
               while (iterB_1 != m_vB_1.end())
               {
                  iterD = m_vD.begin();
                  while (iterD < m_vD.end())
                  {
                     *iterB_2++ = *iterB_1++ * *iterD++;
                  }
               }

               // Solve for Q^H * B
               // Results stored in B
               cpplapack::unmqr('L', 'C', m_lPoints, m_iCoils, nBasis, m_vA, m_lPoints, m_vTau, m_vB_2, m_lPoints, m_vWork, m_vWork.size());
            }

            if (rank < nBasis)
            {
               // R is rank deficient
               // Compute the Z and T matrices such that
               // A * P = (Q1 Q2) * (T 0; 0 0) * Z
               // zgelsy does this before zunmqr, but since it only affects R portion of A
               // it should have not effect on Q and therefore zunmqr as used during iteration
               cpplapack::tzrzf(rank, nBasis, m_vA, m_lPoints, m_vTauZ, m_vWork, m_vWork.size());
            }

            // Solve the linear system
            // B = inv(T) * Q1^H * b
            // Where T == R if rank == nBasis
            cpplapack::trsm('L', 'U', 'N', 'N', rank, m_iCoils, 1.0, m_vA, m_lPoints, m_vB_2, m_lPoints);

            if (rank < nBasis)
            {
               // Null the terms beyond the rank
               iterB_2 = m_vB_2.begin();
               while (iterB_2 < m_vB_2.end())
               {
                  std::fill(iterB_2+rank, iterB_2+nBasis, 0.0);
                  iterB_2 += m_lPoints;
               }

               // Multiply by Z^H
               // B = Z^H * inv(T) * Q1^H * b
               cpplapack::unmrz('L', 'C', nBasis, m_iCoils, rank, nBasis - rank, m_vA, m_lPoints, m_vTauZ, m_vB_2, m_lPoints, m_vWork, m_vWork.size());
            }

            // Undo the pivoting
            iterB_2 = m_vB_2.begin();
            while (iterB_2 < m_vB_2.end())
            {
               // jpvt contains the pivoting in Fortran indexing, i.e. ones based
               // So populate using an address shifted by 1
               typename data_type::iterator iterTmp = m_vWork.begin();
               iterTmp--;
               for (size_t i = 0; i < nBasis; i++)
                  iterTmp[m_vJPVT[i]] = iterB_2[i];
               std::copy(iterTmp, iterTmp+nBasis, iterB_2);
               iterB_2 += m_lPoints;
            }

            // Undo the scaling, but only on B since A is not used anymore
            if (Ascale == 1)
               cpplapack::lascl('G', 0l, 0l, scaleA, smallNumber, nBasis, m_iCoils, m_vB_2, m_lPoints);
            else if (Ascale == 2)
               cpplapack::lascl('G', 0l, 0l, scaleA, bigNumber, nBasis, m_iCoils, m_vB_2, m_lPoints);

            if (Bscale == 1)
               cpplapack::lascl('G', 0l, 0l, smallNumber, scaleB, nBasis, m_iCoils, m_vB_2, m_lPoints);
            else if (Bscale == 2)
               cpplapack::lascl('G', 0l, 0l, bigNumber, scaleB, nBasis, m_iCoils, m_vB_2, m_lPoints);

            // Data required to be stored in PDS
            // m_iFitOrder and m_k used to generate basis
            // coefficients of fit
            // Ellipsoid of validity
            coefficients(m_iCoils, m_vB_2, m_lPoints);

         } catch (std::exception &e)
         {
            return false;
         }
         return true;
      }
   }
}

#endif //RECONTOOLS_B1MAP_HPP
