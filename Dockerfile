FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive

# Install the Kitware APT Repository for latest version of cmake
RUN apt-get update && \
    apt-get -qq -y install apt-transport-https ca-certificates gnupg software-properties-common wget && \
    wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add - && \
    apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'

RUN apt-get update && \
    apt-get -qq -y upgrade cmake git g++ libblas-dev liblapack-dev libboost-dev && \
    apt-get clean
 
